**RWTH Aachen - Deep Learning in Physics Research SS2020**

**Exercise X Topic**

Date: dd.mm.2020

Students:
| student            | rwth ID |
|--------------------|---------|
| Baptiste Corneglio | 411835|
| Florian Stadtmann  | 367319 |
| Johannes Wasmer    |  090800 |
